resource "google_compute_firewall" "http-https" {
  name = "http-https"
  network = "default"

  allow {
   protocol = "tcp"
   ports = ["80", "443"]
  }

 source_ranges = ["0.0.0.0/0"]
}


resource "google_compute_firewall" "udp-ports" {
  name = "udp-ports"
  network = "default"

  allow {
   protocol = "udp"
   ports = ["10000-20000"]
  }

 source_ranges = ["10.0.0.23/32"]
}
